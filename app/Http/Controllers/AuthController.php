<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use App\Models\User;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\ResetPasswordRequest;

class AuthController extends Controller
{
    public function register(RegisterRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return response()->json(['user' => $user,], 200);
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->with('preferredCategories')->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }
        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json(['user' => $user, 'token' => $token], 200);
    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();

        return response()->json('Logout successful', 200);
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->json(['message' => 'User does not exist'], 404);
        }
        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json(['message' => 'Password reset successful'], 200);
    }

    public function checkTokenValidity(Request $request)
    {
        if (Auth::check()) {
            $user = User::with('preferredCategories')->find(Auth::id());
            return response()->json(["msg" => "true", 'user' => $user], 200);
        }
        return response()->json(["msg" => "false"], 401);
    }
}
