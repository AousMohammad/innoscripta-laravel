<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\UserCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function update(Request $request)
    {
        $user=Auth::user();
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
        ]);

        $user->name=$request->name;
        $user->email=$request->email;
        $user->save();
        return response()->json(["data" => $user], 200);
    }

    public function changePassword(Request $request){
        $request->validate([
            'old_password'=>'required',
            'password'=>'required|confirmed',
            'password_confirmation'=>'required'
        ]);
        $user=Auth::user();
        if (!(Hash::check($request->get('old_password'), $user->password))){
            return  response()->json("wrong password",422);
        }
        if (strcmp($request->get('old_password'), $request->get('password')) == 0){
            return response()->json("can not use same password",422);
        }
        $user->password=Hash::make($request->password);
        $user->save();
        return  response()->json("password changed successfully",200);
    }

    public function updateCategories(Request $request){
        UserCategory::where('user_id',Auth::id())->delete();
        $ids=$request->ids;
        foreach ($ids as $id){
            $category=Category::find($id);
            if ($category){
                UserCategory::create([
                    'user_id'=>Auth::id(),
                    'category_id'=>$category->id
                ]);
            }
        }
    }
}
