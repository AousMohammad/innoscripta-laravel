<?php

namespace App\Http\Controllers;

use App\Models\Source;
use Illuminate\Http\Request;

class SourceController extends Controller
{
    public function index()
    {
        $sources = Source::all();
        return response()->json(['sources' => $sources], 200);
    }

    public function show($id)
    {
        $source = Source::find($id);
        return response()->json(['source' => $source], 200);
    }
}
