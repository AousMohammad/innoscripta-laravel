<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    use HasFactory;

    protected $guarded = ['id','created_at','updated_at'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_sources');
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }
}
