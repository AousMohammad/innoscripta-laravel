### Run this commands one by one:

`docker-compose up -d --build`

`docker exec -it app composer install`

`docker exec -it app php artisan migrate:fresh --seed`

`docker exec -it app php artisan optimize`

`docker exec -it app php artisan fetch:articles`

go to `http://localhost:8000`
